#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "delegates/printerstatusdelegate.h"
#include "delegates/nofocusrectdelegate.h"
#include <QDebug>
#include <QDesktopServices>

MainWindow::MainWindow(const QUrl &lpHost, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    printerModel = new PrinterModel;
    jobAllModel = new JobModel;
    jobMineModel = new JobModel;

    ui->setupUi(this);
//    ui->statusListView->setAutoFillBackground(false);
//    ui->statusListView-> setStyleSheet("background-color: transparent;");
//    ui->statusListView->viewport()->setAutoFillBackground(false);
//    ui->statusListView->viewport()->setStyleSheet("background-color: transparent;");
    ui->statusListView->setItemDelegate(new PrinterStatusDelegate);
    ui->statusListView->setModel(printerModel);
    ui->statusListView->setSelectionMode(QAbstractItemView::NoSelection);

    jobAllSortModel = new JobProxySortFilterModel(this);
    jobAllSortModel->setSourceModel(jobAllModel);
    jobAllSortModel->setSortRole(Qt::EditRole);
    jobAllSortModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    jobAllSortModel->setDynamicSortFilter(true);
    jobAllSortModel->setFilterKeyColumn(3);
    jobMineSortModel = new JobProxySortFilterModel(this);
    jobMineSortModel->setSourceModel(jobMineModel);
    jobMineSortModel->setSortRole(Qt::EditRole);
    jobMineSortModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    jobMineSortModel->setDynamicSortFilter(true);
    jobMineSortModel->setFilterKeyColumn(3);

    ui->jobListMine->setModel(jobMineSortModel);
    ui->jobListMine->setItemDelegate(new NoFocusRectDelegate);
    ui->jobListMine->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->jobListMine->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->jobListMine->sortByColumn(6, Qt::DescendingOrder);
    ui->jobListMine->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->jobListMine->horizontalHeader()->setSectionHidden(1, true);
    ui->jobListMine->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->jobListMine->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->jobListMine->horizontalHeader()->setSectionHidden(4, true);
    ui->jobListMine->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    ui->jobListMine->horizontalHeader()->setSectionResizeMode(6, QHeaderView::ResizeToContents);

    ui->jobListAll->setModel(jobAllSortModel);
    ui->jobListAll->setItemDelegate(new NoFocusRectDelegate);
    ui->jobListAll->setSelectionMode(QAbstractItemView::NoSelection);
    ui->jobListAll->sortByColumn(6, Qt::DescendingOrder);
    ui->jobListAll->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->jobListAll->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->jobListAll->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->jobListAll->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->jobListAll->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    ui->jobListAll->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    ui->jobListAll->horizontalHeader()->setSectionResizeMode(6, QHeaderView::ResizeToContents);

    mRefreshThrottle = new QTimer;
    mRefreshThrottle->setInterval(1000);
    mRefreshThrottle->setSingleShot(true);

    mRefreshTimer = new QTimer;
    mRefreshTimer->setInterval(12000);
    mRefreshTimer->setSingleShot(true);

    client = new LpClient(lpHost);

    connect(mRefreshThrottle, SIGNAL(timeout()), this, SLOT(refreshButtonAvailable()));
    connect(mRefreshTimer, SIGNAL(timeout()), this, SLOT(refreshClient()));
    connect(client, SIGNAL(refreshed()), this, SLOT(refreshClientDone()));

    connect(ui->resetAllPrinters, SIGNAL(clicked()), printerModel, SLOT(uncheckAll()));
    connect(ui->buttonRefresh, SIGNAL(clicked()), this, SLOT(refreshClient()));
    connect(ui->buttonCancel, SIGNAL(clicked()), this, SLOT(cancelJobs()));
    connect(ui->buttonRestart, SIGNAL(clicked()), this, SLOT(restartJobs()));
    connect(ui->jobListMine->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
            this, SLOT(updateUi()));
    connect(ui->statusListView->model(), SIGNAL(dataChanged(QModelIndex, QModelIndex)), this,
            SLOT(updatePrinterFilter(QModelIndex, QModelIndex)));
    connect(ui->statusListView, SIGNAL(doubleClicked(QModelIndex)), this,
            SLOT(togglePrinter(QModelIndex)));
    connect(ui->jobListAll, SIGNAL(doubleClicked(QModelIndex)), this,
            SLOT(jobViewInteract(QModelIndex)));
    connect(ui->jobListMine, SIGNAL(doubleClicked(QModelIndex)), this,
            SLOT(jobViewInteract(QModelIndex)));

    QAction *aQuit = new QAction(this);
    aQuit->setShortcut(Qt::Key_Q | Qt::CTRL);
    connect(aQuit, SIGNAL(triggered()), this, SLOT(close()));
    this->addAction(aQuit);

    refreshClient();
    updateUi();
}

MainWindow::~MainWindow()
{
    delete ui;
    client->deleteLater();
    mRefreshThrottle->deleteLater();
    mRefreshTimer->deleteLater();
    jobMineSortModel->deleteLater();
    jobMineModel->deleteLater();
    jobAllSortModel->deleteLater();
    jobAllModel->deleteLater();
    printerModel->deleteLater();
}

void MainWindow::updateUi()
{
    bool enabled = !ui->jobListMine->selectionModel()->selectedRows().empty();
    ui->buttonCancel->setEnabled(enabled);
    ui->buttonRestart->setEnabled(enabled);
}

void MainWindow::refreshButtonAvailable()
{
    ui->buttonRefresh->setEnabled(true);
}

void MainWindow::refreshClient()
{
    mRefreshTimer->stop();
    ui->buttonRefresh->setEnabled(false);
    client->refreshAll();
}

void MainWindow::refreshClientDone()
{
    // save selection, update printers, restore
    const QStringList &printerNames = printerModel->selectedPrinters().toList();
    printerModel->setPrinterList(client->allPrinters());
    printerModel->check(printerNames);

    // update all jobs
    jobAllModel->setJobList(client->allJobs());

    // save selection, update my jobs, restore
    const QList<Job> &jobs = selectedJobMine();
    jobMineModel->setJobList(client->myJobs());
    ui->jobListMine->selectionModel()->select(jobMineModel->selectionForJobs(jobs),
            QItemSelectionModel::ClearAndSelect);

    // update UI
    updateJobCounts();
    // start again
    mRefreshTimer->start();
}

void MainWindow::togglePrinter(QModelIndex index)
{
    printerModel->setData(index, true, Qt::CheckStateRole);
}

void MainWindow::updatePrinterFilter(QModelIndex topLeft, QModelIndex bottomRight)
{
    Q_UNUSED(topLeft);
    Q_UNUSED(bottomRight);
    QSet<QString> printerNames = printerModel->selectedPrinters();
    ui->resetAllPrinters->setVisible(!printerNames.isEmpty());
    jobAllSortModel->setFilterNames(printerNames);
    jobMineSortModel->setFilterNames(printerNames);
    updateJobCounts();
}

void MainWindow::updateJobCounts()
{
    int total, showed;
    QString stat;

    // mine
    total = jobMineSortModel->sourceModel()->rowCount();
    showed = jobMineSortModel->rowCount();
    if (total != showed)
        stat = tr("%n élément(s) affiché(s) sur %1", "", showed).arg(total);
    else
        stat = tr("%n élément(s)", "", showed);
    ui->jobCountMine->setText(stat);

    // all
    total = jobAllSortModel->sourceModel()->rowCount();
    showed = jobAllSortModel->rowCount();
    if (total != showed)
        stat = tr("%n élément(s) affiché(s) sur %1", "", showed).arg(total);
    else
        stat = tr("%n élément(s)", "", showed);
    ui->jobCountAll->setText(stat);
}

QList<Job> MainWindow::selectedJobMine() const
{
    QList<Job> jobs;
    QModelIndexList indexes = ui->jobListMine->selectionModel()->selectedRows(1);
    foreach (QModelIndex index, indexes)
        jobs.append(jobMineModel->jobById(index.data().toInt()));
    return jobs;
}

void MainWindow::restartJobs()
{
    foreach (Job job, selectedJobMine())
        client->jobRestart(job);
}

void MainWindow::cancelJobs()
{
    foreach (Job job, selectedJobMine())
        client->jobCancel(job);
}

void MainWindow::jobViewInteract(QModelIndex index)
{
    if (index.column() == 4) {
        // user
        const QString &username = ((QTableView *) sender())->model()->data(index).toString();
        if (username.isEmpty())
            return;
        QDesktopServices::openUrl(QUrl("http://trombi.eirb.fr/profil/" + QUrl::toPercentEncoding(
                                           username)));

    } else if (index.column() == 3) {
        // printer
        const QString &printer = ((QTableView *) sender())->model()->data(index).toString();
        printerModel->check(printer);

    }
}
