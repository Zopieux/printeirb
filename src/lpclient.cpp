#include "lpclient.h"
#include "job.h"
#include "printer.h"
#include "cups/cups.h"
#include <unicode/ucsdet.h>
#include <cstdio>
#include <QThread>
#include <QTextCodec>
#include <QProcess>


LpClient::LpClient(const QUrl &lpHost, QObject *parent) :
    QObject(parent), lpHost(lpHost), mPrinters(), mJobs()
{
    worker = new ClientRefreshWorker;
    worker->moveToThread(&workerThread);
    connect(&workerThread, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(worker, SIGNAL(resultReady(MapPrinters, MapJobs)), this, SLOT(workerResult(MapPrinters,
            MapJobs)));
    connect(this, SIGNAL(_doRefresh(LpClient *)), worker, SLOT(run(LpClient *)));
    workerThread.start();
}

LpClient::~LpClient()
{
    workerThread.quit();
    workerThread.wait();
}

void LpClient::cupsConnect()
{
    cupsSetServer(QString("%1:%2").arg(lpHost.host()).arg(lpHost.port()).toLocal8Bit());
}

MapJobs LpClient::allJobs() const
{
    return mJobs;
}

MapJobs LpClient::myJobs() const
{
    MapJobs jobs;
    Job job;
    foreach (JobId jobName, mJobs.keys()) {
        job = mJobs[jobName];
        if (job.isMine && job.status == Job::PENDING)
            jobs[jobName] = job;
    }
    return jobs;
}

MapPrinters LpClient::allPrinters() const
{
    return mPrinters;
}

static Job::JobStatus ippStateToJobState(ipp_jstate_t state)
{
    switch (state) {
    case IPP_JOB_HELD:
    case IPP_JOB_PENDING:
    case IPP_JOB_PROCESSING:
        return Job::PENDING;
    case IPP_JOB_ABORTED:
    case IPP_JOB_STOPPED:
        return Job::ABORTED;
    case IPP_JOB_CANCELED:
        return Job::CANCELED;
    case IPP_JOB_COMPLETED:
        return Job::COMPLETED;
    }
    return Job::COMPLETED;
}

void LpClient::refreshAll()
{
    emit _doRefresh(this);
}

void LpClient::workerResult(MapPrinters printers, MapJobs jobs)
{
    mPrinters.swap(printers);
    mJobs.swap(jobs);
    emit refreshed();
}


bool LpClient::jobCancel(Job job)
{
    // TODO: put in thread
    cupsConnect();
    ipp_t *request;
    char uri[HTTP_MAX_URI];
    request = ippNewRequest(IPP_CANCEL_JOB);
    std::sprintf(uri, "ipp://localhost/jobs/%d", job.id);
    ippAddString(request, IPP_TAG_OPERATION, IPP_TAG_URI,
                 "job-uri", NULL, uri);
    ippAddString(request, IPP_TAG_OPERATION, IPP_TAG_NAME,
                 "requesting-user-name", NULL, cupsUser());
    ippAddBoolean(request, IPP_TAG_OPERATION, "purge-jobs", 1);
    ippDelete(cupsDoRequest(CUPS_HTTP_DEFAULT, request, "/jobs/"));
    if (cupsLastError() > IPP_OK_CONFLICT) {
        qWarning() << "jobCancel() error:" << cupsLastErrorString();
        return false;
    }
    return true;
}

bool LpClient::jobRestart(Job job)
{
    // TODO: put in thread
//    cups_option_t *options;
//    int numOptions = 0;
//    numOptions = cupsAddOption("job-hold-until", "no-hold", numOptions, &options);
//    numOptions = cupsAddOption("job-priority", "100", numOptions, &options);
//    Q_ASSERT(numOptions == 2);
//    cupsEncodeOptions(request, numOptions, options);
    cupsConnect();
    ipp_t *request;
    char uri[HTTP_MAX_URI];
    request = ippNewRequest(IPP_RESTART_JOB);
    std::sprintf(uri, "ipp://localhost/jobs/%d", job.id);
    ippAddString(request, IPP_TAG_OPERATION, IPP_TAG_URI,
                 "job-uri", NULL, uri);
    ippAddString(request, IPP_TAG_OPERATION, IPP_TAG_NAME,
                 "requesting-user-name", NULL, cupsUser());
    ippDelete(cupsDoRequest(CUPS_HTTP_DEFAULT, request, "/jobs"));
    if (cupsLastError() > IPP_OK_CONFLICT) {
        qWarning() << "jobRestart() error:" << cupsLastErrorString();
        return false;
    }
    return true;
}

void ClientRefreshWorker::run(LpClient *client)
{
    QMap<QString, Printer> printers;
    QMap<JobId, Job> jobs;
    cups_dest_t *dests;
    qDebug() << "refresh";
    client->cupsConnect();
    int count = cupsGetDests(&dests);
    if (count == -1) {
        qWarning() << "refresh (dests) error:" << cupsLastErrorString();
        emit failed();
        return;
    }
    cups_dest_t *dest = dests;
    for (int i = 0; i < count; ++i, ++dest) {
        QString name = QString::fromLocal8Bit(dest->name).toLower();
        QString location = QString::fromLocal8Bit(cupsGetOption("printer-location", dest->num_options,
                           dest->options));
        bool available = QString::fromLocal8Bit(cupsGetOption("printer-is-accepting-jobs",
                                                dest->num_options, dest->options)) == "true";
        QStringList reasons = QString::fromLocal8Bit(cupsGetOption("printer-state-reasons",
                              dest->num_options, dest->options)).split(',');
        reasons.removeDuplicates();
        reasons.removeAll("none");
        printers[name] = Printer {
            .name = name,
            .location = location,
            .available = available,
            .errorReasons = reasons,
            .queueSize = 0,
        };
    }
    cupsFreeDests(count, dests);
    cups_job_t *cjobs;
    count = cupsGetJobs(&cjobs, NULL, 0, CUPS_WHICHJOBS_ALL);
    cups_job_t *cjob = cjobs;
    if (count < 0) {
        qWarning() << "refresh (jobs) error:" << cupsLastErrorString() << cupsLastError();
        emit failed();
        return;
    }
    qDebug() << count << "jobs found";
    QString selfUser = QString::fromLocal8Bit(cupsUser());
    QString printer, user;
    Job::JobStatus state;
    for (int i = 0; i < count; ++i, ++cjob) {
        printer = QString::fromLocal8Bit(cjob->dest).toLower();
        if (!printers.contains(printer)) {
            qWarning() << "found a job with unknown printer:" << printer;
            continue;
        }
        state = ippStateToJobState(cjob->state);
        user = QString::fromLocal8Bit(cjob->user);
        UErrorCode status = U_ZERO_ERROR;
        UCharsetDetector *detector = ucsdet_open(&status);
        size_t length = strlen(cjob->title);
        ucsdet_setText(detector, cjob->title, length, &status);
        const UCharsetMatch *match = ucsdet_detect(detector, &status);
        ucsdet_close(detector);
        QString title;
        if (match == NULL) {
            title = QString::fromLatin1(cjob->title);
        } else {
            const char *codecName = ucsdet_getName(match, &status);
            QTextCodec *codec = QTextCodec::codecForName(codecName);
            if (codec == NULL || U_FAILURE(status)) {
                title = QString::fromLatin1(cjob->title);
            } else {
                title = codec->toUnicode(cjob->title, length);
            }
        }
        jobs[cjob->id] = Job {
            .status = state,
            .id = cjob->id,
            .size = cjob->size,
            .printer = printer,
            .username = user,
            .title = title,
            .datetime = QDateTime::fromTime_t(cjob->creation_time),
            .isMine = user == selfUser,
        };
        if (state == Job::PENDING)
            printers[printer].queueSize++;
    }
    cupsFreeJobs(count, cjobs);
    emit resultReady(printers, jobs);
}
