#ifndef PRINTERSTATUSDELEGATE_H
#define PRINTERSTATUSDELEGATE_H

#include <QStyledItemDelegate>
#include <QPainter>

class PrinterStatusDelegate : public QStyledItemDelegate
{
public:
    PrinterStatusDelegate();
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const;
protected:
    static int margin, height;
};

#endif // PRINTERSTATUSDELEGATE_H
