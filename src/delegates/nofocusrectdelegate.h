#ifndef NOFOCUSDELEGATE_H
#define NOFOCUSDELEGATE_H

#include <QItemDelegate>

class NoFocusRectDelegate : public QItemDelegate
{
public:
    void drawFocus(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect) const;
};

#endif // NOFOCUSDELEGATE_H
