#include "nofocusrectdelegate.h"

void NoFocusRectDelegate::drawFocus(QPainter *painter, const QStyleOptionViewItem &option,
                                    const QRect &rect) const
{
    QStyleOptionViewItem option2(option);
    option2.state &= ~QStyle::State_HasFocus;
    QItemDelegate::drawFocus(painter, option2, rect);
}
