#include "printerstatusdelegate.h"
#include "models/printermodel.h"
#include "utils.h"

int PrinterStatusDelegate::margin = 4;
int PrinterStatusDelegate::height = 24;

PrinterStatusDelegate::PrinterStatusDelegate()
{
}

void PrinterStatusDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
{
    painter->save();
    painter->setRenderHint(QPainter::SmoothPixmapTransform);

    QStyleOptionViewItem options = option;
    initStyleOption(&options, index);
    options.text = "";
    options.features &= ~QStyleOptionViewItem::HasCheckIndicator;
    if (index.data(Qt::CheckStateRole) == Qt::Checked) {
        options.backgroundBrush = QColor::fromHslF(0.55f, 0.3f, 0.7f);
//        options.state |= QStyle::State_HasFocus;
    } else {
    }
    options.state &= ~QStyle::State_HasFocus;
    options.widget->style()->drawControl(QStyle::CE_ItemViewItem, &options, painter);

    bool valid = index.data(PrinterModel::AvailableRole).toBool();

    QRect r(option.rect);
    r.setSize(QSize(height, height));
    r.translate(margin, margin);
    painter->drawPixmap(r, QPixmap(index.data(PrinterModel::QueueSizeRole).toInt() > 0 ?
                                   ":/img/working" : valid ? ":/img/valid" : ":/img/invalid"));

    r.translate(height + margin * 2, -margin / 2);
    r.setRight(option.rect.right());
    QRect rt(r);
//    rt.setHeight(rt.height() / 2);
    QFont titleFont(painter->font());
    titleFont.setBold(true);
    titleFont.setPointSizeF(titleFont.pointSizeF() * 0.9);
    QFont locFont(painter->font());
    locFont.setPointSizeF(locFont.pointSizeF() * 0.75);

    painter->setFont(titleFont);
    painter->drawText(rt, Qt::AlignTop,
                      Utils::toTitleCase(index.data(PrinterModel::NameRole).toString()));

    rt.translate(0, rt.height() / 2 + 2);
    painter->setFont(locFont);
    painter->drawText(rt, Qt::AlignTop,
                      Utils::toTitleCase(index.data(PrinterModel::LocationRole).toString()));

    painter->restore();
}

QSize PrinterStatusDelegate::sizeHint(const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
{
    Q_UNUSED(index);
    return QSize(option.rect.width(), height + margin * 2);
}
