#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QTimer>
#include <QFuture>
#include <QSortFilterProxyModel>
#include "lpclient.h"
#include "models/jobmodel.h"
#include "models/printermodel.h"
#include "models/jobproxysortfiltermodel.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const QUrl &lpHost, QWidget *parent = 0);
    ~MainWindow();

protected slots:
    void updateUi();
    void refreshButtonAvailable();
    void refreshClient();
    void refreshClientDone();
    void togglePrinter(QModelIndex);
    void jobViewInteract(QModelIndex);
    void updatePrinterFilter(QModelIndex, QModelIndex);
    void updateJobCounts();
    void restartJobs();
    void cancelJobs();

private:
    Ui::MainWindow *ui;
    LpClient *client;
    QTimer *mRefreshThrottle, *mRefreshTimer;
    JobModel *jobMineModel, *jobAllModel;
    JobProxySortFilterModel *jobAllSortModel, *jobMineSortModel;
    PrinterModel *printerModel;

    QList<Job> selectedJobMine() const;
};

#endif // MAINWINDOW_H
