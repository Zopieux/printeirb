#ifndef PRINTER_H
#define PRINTER_H

#include <QString>
#include <QDebug>

struct Printer {
    QString name;
    QString location;
    bool available;
    QStringList errorReasons;
    int queueSize;

public:
    friend QDebug operator<<(QDebug d, const Printer &p)
    {
        d << p.name << " (";
        d.nospace() << p.available << ")";
        return d;
    }
};

typedef QMap<QString, Printer> MapPrinters;
Q_DECLARE_METATYPE(MapPrinters)

#endif // PRINTER_H
