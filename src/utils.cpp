#include "utils.h"
#include <QStringList>
#include <QTranslator>

namespace Utils
{

QString toTitleCase(const QString &in)
{
    if (in.size() <= 1)
        return in.toUpper();
    return in.at(0).toUpper() + in.mid(1).toLower();
}

QString byteSizeHuman(int size)
{
    float num = size;
    QStringList list;
    list << "KB" << "MB" << "GB" << "TB";

    QStringListIterator i(list);
    QString unit("B");

    while (num >= 1024.0 && i.hasNext()) {
        unit = i.next();
        num /= 1024.0;
    }
    return QString().setNum(num, 'f', 2) + " " + unit;
}

QString timeAgoHuman(const QDateTime &date)
{
    qint64 seconds = date.secsTo(QDateTime::currentDateTimeUtc());
    if (seconds < 0 || seconds > 3600 * 24 * 7)
        return date.toString();

    QStringList list, res;
    list << "sec" << "min" << "h" << "j";
    QList<int> divs;
    divs << 60 << 60 << 24 << 7;
    QStringListIterator i(list);
    QListIterator<int> d(divs);
    QString r;
    qint64 val;
    while (seconds >= 0 && i.hasNext()) {
        val = seconds % d.peekNext();
        if (val > 0) {
            r = QString().setNum(val) + ' ' + i.peekNext();
//            if (seconds > 1)
//                r += 's';
            res.push_front(r);
        }
        seconds /= d.peekNext();
        i.next();
        d.next();
    }
    return res.join(", ");
}

}
