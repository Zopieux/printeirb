#ifndef JOB_H
#define JOB_H

#include <QString>
#include <QDateTime>

typedef int JobId;
typedef int JobSize;

struct Job {
    enum JobStatus { PENDING, COMPLETED, ABORTED, CANCELED } status;
    JobId id;
    JobSize size;
    QString printer;
    QString username;
    QString title;
    QDateTime datetime;
    bool isMine;
};

typedef QMap<JobId, Job> MapJobs;
Q_DECLARE_METATYPE(MapJobs)

#endif // JOB_H
