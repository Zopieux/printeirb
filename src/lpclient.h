#ifndef LPCLIENT_H
#define LPCLIENT_H

#include <QObject>
#include <QThread>
#include <QUrl>
#include "job.h"
#include "printer.h"

class LpClient;


class ClientRefreshWorker : public QObject
{
    Q_OBJECT
public slots:
    void run(LpClient *);

signals:
    void failed();
    void resultReady(MapPrinters, MapJobs);
};


class LpClient : public QObject
{
    Q_OBJECT
public:
    explicit LpClient(const QUrl &lpHost, QObject *parent = 0);
    ~LpClient();
    MapPrinters allPrinters() const;
    MapJobs allJobs() const;
    MapJobs myJobs() const;

    void cupsConnect();

signals:
    void refreshed();
    void _doRefresh(LpClient *);

protected slots:
    void workerResult(MapPrinters, MapJobs);

public slots:
    void refreshAll();
    bool jobRestart(Job job);
    bool jobCancel(Job job);

private:
    QUrl lpHost;
    QThread workerThread;
    ClientRefreshWorker *worker;
    MapPrinters mPrinters;
    MapJobs mJobs;

};


#endif // LPCLIENT_H
