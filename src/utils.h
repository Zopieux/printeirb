#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QDateTime>

namespace Utils
{

QString toTitleCase(const QString &in);
QString byteSizeHuman(int size);
QString timeAgoHuman(const QDateTime &date);

}

#endif // UTILS_H
