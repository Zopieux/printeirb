#include "mainwindow.h"
#include "lpclient.h"
#include <QApplication>
#include <QTranslator>
#include <QUrl>
#include <iostream>

static void printUsage(QApplication &a)
{
    std::cout << "Usage: " << a.arguments().first().toStdString() << " host[:port]" << std::endl;
}

int main(int argc, char *argv[])
{
    qRegisterMetaType<MapPrinters>("MapPrinters");
    qRegisterMetaType<MapJobs>("MapJobs");

    QApplication a(argc, argv);

    QTranslator translator;
    translator.load("printeirb_fr", a.applicationDirPath());
    a.installTranslator(&translator);

    QUrl lpHost;
    if (a.arguments().size() != 2) {
        lpHost.setHost("lppeda.pedago.ipb.fr");
    } else {
        lpHost = QUrl(a.arguments().at(1), QUrl::StrictMode);
    }
    if (!lpHost.isValid() || lpHost.host().isEmpty()) {
        printUsage(a);
        return 1;
    }
    if (lpHost.port() == -1)
        lpHost.setPort(631);

    MainWindow w(lpHost);
    w.show();

    return a.exec();
}
