#include "printermodel.h"
#include "utils.h"

PrinterModel::PrinterModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

int PrinterModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mPrinters.size();
}

QVariant PrinterModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const QString &name = mPrintersId.at(index.row());
    Printer p = mPrinters[name];

    if (role == NameRole) {
        return p.name;
    } else if (role == LocationRole) {
        return p.location;
    } else if (role == AvailableRole) {
        return p.available;
    } else if (role == QueueSizeRole) {
        return p.queueSize;
    } else if (role == ErrorReasonsRole) {
        return p.errorReasons;
    }

    if (role == Qt::ToolTipRole) {
        QString msg = p.queueSize > 0 ? tr("%n tâche(s) en attente ou en traitement",
                                           "", p.queueSize) :
                      p.available ? tr("Prête à imprimer") : tr("Indisponible ou en panne");
        if (!p.errorReasons.isEmpty())
            msg += "\n" + tr("%n problème(s) : %1", "", p.errorReasons.size()).arg(p.errorReasons.join(", "));
        return msg;
    }

    if (role == Qt::CheckStateRole) {
        return mSelected.value(name, Qt::Unchecked);
    }

    return QVariant();
}

Qt::ItemFlags PrinterModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
}

void PrinterModel::setPrinterList(MapPrinters printers)
{
    QList<QString> keys = printers.keys();
    emit beginResetModel();
    mPrinters.swap(printers);
    mPrintersId.swap(keys);
    mSelected.clear();
    qSort(mPrintersId);
    foreach (QString name, mPrintersId)
        mSelected[name] = Qt::Unchecked;
    emit endResetModel();
}

bool PrinterModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(value);
    if (role == Qt::CheckStateRole && index.isValid()) {
        const QString &name = index.data(NameRole).toString();
        mSelected[name] = mSelected[name] == Qt::Checked ? Qt::Unchecked : Qt::Checked;
        emit dataChanged(index, index, QVector<int>() << Qt::CheckStateRole);
        return true;
    }
    return false;
}

QSet<QString> PrinterModel::selectedPrinters() const
{
    QSet<QString> ret;
    QMap<QString, Qt::CheckState>::const_iterator it;
    for (it = mSelected.cbegin(); it != mSelected.cend(); ++it)
        if (it.value() == Qt::Checked)
            ret.insert(it.key());
    return ret;
}

void PrinterModel::check(const QStringList &names)
{
    QMap<QString, Qt::CheckState>::iterator it;
    for (it = mSelected.begin(); it != mSelected.end(); ++it) {
        bool found = false;
        foreach (QString name, names) {
            if (QString::compare(it.key(), name, Qt::CaseInsensitive) == 0) {
                found = true;
                break;
            }
        }
        it.value() = found ? Qt::Checked : Qt::Unchecked;
    }
    emit dataChanged(index(0, 0), index(rowCount() - 1, 1), QVector<int>() << Qt::CheckStateRole);
}

void PrinterModel::check(const QString &name)
{
    QMap<QString, Qt::CheckState>::iterator it;
    for (it = mSelected.begin(); it != mSelected.end(); ++it) {
        it.value() = QString::compare(it.key(), name,
                                      Qt::CaseInsensitive) == 0 ? Qt::Checked : Qt::Unchecked;
    }
    emit dataChanged(index(0, 0), index(rowCount() - 1, 1), QVector<int>() << Qt::CheckStateRole);
}

void PrinterModel::uncheckAll()
{
    QMap<QString, Qt::CheckState>::iterator it;
    for (it = mSelected.begin(); it != mSelected.end(); ++it)
        it.value() = Qt::Unchecked;
    emit dataChanged(index(0, 0), index(rowCount() - 1, 1), QVector<int>() << Qt::CheckStateRole);
}
