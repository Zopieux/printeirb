#ifndef PRINTERMODEL_H
#define PRINTERMODEL_H

#include <QAbstractListModel>
#include <QSet>
#include "lpclient.h"
#include "printer.h"

class PrinterModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        NameRole = Qt::DisplayRole,
        LocationRole = Qt::UserRole,
        AvailableRole, QueueSizeRole, ErrorReasonsRole
    };

    explicit PrinterModel(QObject *parent = 0);
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QSet<QString> selectedPrinters() const;

signals:

public slots:
    void setPrinterList(MapPrinters printers);
    void check(const QStringList &names);
    void check(const QString &name);
    void uncheckAll();

private:
    MapPrinters mPrinters;
    QList<QString> mPrintersId;
    QMap<QString, Qt::CheckState> mSelected;
};

#endif // PRINTERMODEL_H
