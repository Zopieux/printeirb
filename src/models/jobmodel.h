#ifndef JOBMODEL_H
#define JOBMODEL_H

#include <QAbstractTableModel>
#include <QItemSelection>
#include "job.h"
#include "lpclient.h"

class JobModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit JobModel(QObject *parent = 0);
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    Job jobById(JobId id) const;
    QItemSelection selectionForJobs(const QList<Job> &jobs) const;

signals:

public slots:
    void setJobList(MapJobs jobs);

private:
    MapJobs mJobs;
    QList<JobId> mJobsId;

};

#endif // JOBMODEL_H
