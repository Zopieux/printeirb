#include "jobproxysortfiltermodel.h"

JobProxySortFilterModel::JobProxySortFilterModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

void JobProxySortFilterModel::setFilterNames(const QSet<QString> &names)
{
    mNames.clear();
    foreach (QString name, names) {
        mNames.insert(name.toLower());
    }
    invalidateFilter();
}

bool JobProxySortFilterModel::filterAcceptsRow(int source_row,
        const QModelIndex &source_parent) const
{
    if (mNames.isEmpty())
        return true;
    QModelIndex index = sourceModel()->index(source_row, filterKeyColumn(), source_parent);
    return mNames.contains(index.data(filterRole()).toString().toLower());
}
