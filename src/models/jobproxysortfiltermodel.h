#ifndef JOBPROXYSORTFILTERMODEL_H
#define JOBPROXYSORTFILTERMODEL_H

#include <QSortFilterProxyModel>
#include <QSet>

class JobProxySortFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT;
public:
    explicit JobProxySortFilterModel(QObject *parent = 0);
    void setFilterNames(const QSet<QString> &names);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

private:
    QSet<QString> mNames;
};

#endif // JOBPROXYSORTFILTERMODEL_H
