#include "jobmodel.h"
#include "utils.h"
#include <QPixmap>

JobModel::JobModel(QObject *parent) :
    QAbstractTableModel(parent), mJobs()
{
}

int JobModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mJobs.size();
}

int JobModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 7;
}

QVariant JobModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row(), col = index.column();
    Job job = mJobs[mJobsId.at(row)];

    if (role == Qt::DisplayRole) {
        if (col == 0) {
            Job::JobStatus status = job.status;
            return status == Job::PENDING ? tr("Attente") :
                   status == Job::COMPLETED ? tr("Terminé") :
                   status == Job::ABORTED ? tr("Avorté") :
                   status == Job::CANCELED ? tr("Annulé") :
                   "?";
        } else if (col == 1) {
            return job.id;
        } else if (col == 2) {
            return Utils::byteSizeHuman(job.size);
        } else if (col == 3) {
            return Utils::toTitleCase(job.printer);
        } else if (col == 4) {
            return job.username.toLower();
        } else if (col == 5) {
            return job.title;
        } else if (col == 6) {
            return Utils::timeAgoHuman(job.datetime);
        }
    } else if (role == Qt::DecorationRole) {
        if (col == 0) {
            return QPixmap(QStringLiteral(":/img/state/") + (job.status == Job::PENDING ? "pending" : job.status
                           == Job::COMPLETED ? "done" : "error"));
        }
    } else if (role == Qt::ToolTipRole) {
        if (col == 3)
            return tr("Double-cliquer pour filtrer sur cette imprimante.");
        else if (col == 4)
            return tr("Double-cliquer pour chercher cette personne sur le Trombi de l'eirb.");
        else if (col == 6)
            return job.datetime.toString(QStringLiteral("ddd dd MMM yyyy à HH:mm:ss"));
    } else if (role == Qt::EditRole) {
        if (col == 0)
            return job.status;
        else if (col == 2)
            return job.size;
        else if (col == 6)
            return job.datetime;
        else
            return data(index, Qt::DisplayRole);
    }

    return QVariant();
}

QVariant JobModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        if (section == 0)
            return tr("Statut");
        else if (section == 1)
            return tr("Id");
        else if (section == 2)
            return tr("Taille");
        else if (section == 3)
            return tr("Impr.");
        else if (section == 4)
            return tr("Utilisateur");
        else if (section == 5)
            return tr("Nom");
        else if (section == 6)
            return tr("Date");
    }

    if (role == Qt::ToolTipRole) {
        if (section == 3)
            return tr("Imprimante");
    }

    return QVariant();
}

Qt::ItemFlags JobModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index);
}

void JobModel::setJobList(MapJobs jobs)
{
    QList<JobId> keys = jobs.keys();
    emit beginResetModel();
    mJobs.swap(jobs);
    mJobsId.swap(keys);
    emit endResetModel();
}

Job JobModel::jobById(JobId id) const
{
    return mJobs[id];
}

QItemSelection JobModel::selectionForJobs(const QList<Job> &jobs) const
{
    QItemSelection selection;
    foreach (Job job, jobs) {
        int row = mJobsId.indexOf(job.id);
        if (row != -1)
            selection.append(QItemSelectionRange(index(row, 0), index(row, columnCount() - 1)));
    }
    return selection;
}
