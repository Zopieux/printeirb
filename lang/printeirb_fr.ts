<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>JobModel</name>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="33"/>
        <source>Attente</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="34"/>
        <source>Terminé</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="35"/>
        <source>Avorté</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="36"/>
        <source>Annulé</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="58"/>
        <source>Double-cliquer pour filtrer sur cette imprimante.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="60"/>
        <source>Double-cliquer pour chercher cette personne sur le Trombi de l&apos;eirb.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="85"/>
        <source>Statut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="87"/>
        <source>Id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="89"/>
        <source>Taille</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="91"/>
        <source>Imprimante</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="93"/>
        <source>Utilisateur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="95"/>
        <source>Nom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="97"/>
        <source>Date</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="92"/>
        <source>Mes tâches</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="98"/>
        <source>Actualiser</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="160"/>
        <source>Annuler</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="258"/>
        <source>Toutes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="143"/>
        <source>Réessayer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="198"/>
        <source>Toutes les tâches</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="27"/>
        <source>État des imprimantes</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/mainwindow.cpp" line="166"/>
        <location filename="../src/mainwindow.cpp" line="175"/>
        <source>%n élément(s) affiché(s) sur %1</source>
        <translation>
            <numerusform>%n élément affiché sur %1</numerusform>
            <numerusform>%n éléments affichés sur %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/mainwindow.cpp" line="168"/>
        <location filename="../src/mainwindow.cpp" line="177"/>
        <source>%n élément(s)</source>
        <translation>
            <numerusform>%n élément</numerusform>
            <numerusform>%n éléments</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>PrinterModel</name>
    <message numerus="yes">
        <location filename="../src/models/printermodel.cpp" line="36"/>
        <source>%n tâche(s) en attente ou en traitement</source>
        <translation>
            <numerusform>%n tâche en attente ou en traitement</numerusform>
            <numerusform>%n tâches en attente ou en traitement</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/models/printermodel.cpp" line="38"/>
        <source>Prête à imprimer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/printermodel.cpp" line="38"/>
        <source>Indisponible ou en panne</source>
        <translation></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/models/printermodel.cpp" line="40"/>
        <source>%n problème(s) : %1</source>
        <oldsource>Problème(s) : %1</oldsource>
        <translation>
            <numerusform>%n problème : %1</numerusform>
            <numerusform>%n problèmes : %1</numerusform>
        </translation>
    </message>
</context>
</TS>
