<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>JobModel</name>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="33"/>
        <source>Attente</source>
        <translation>Pending</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="34"/>
        <source>Terminé</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="35"/>
        <source>Avorté</source>
        <translation>Aborted</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="36"/>
        <source>Annulé</source>
        <translation>Canceled</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="58"/>
        <source>Double-cliquer pour filtrer sur cette imprimante.</source>
        <translation>Double-click to filter this printer.</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="60"/>
        <source>Double-cliquer pour chercher cette personne sur le Trombi de l&apos;eirb.</source>
        <translation>Double-click to lookup this person on Trombi de l&apos;eirb.</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="85"/>
        <source>Statut</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="87"/>
        <source>Id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="89"/>
        <source>Taille</source>
        <translation>Size</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="91"/>
        <source>Imprimante</source>
        <translation>Printer</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="93"/>
        <source>Utilisateur</source>
        <translation>User</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="95"/>
        <source>Nom</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/models/jobmodel.cpp" line="97"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="92"/>
        <source>Mes tâches</source>
        <translation>My jobs</translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="98"/>
        <source>Actualiser</source>
        <translation>Refresh</translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="160"/>
        <source>Annuler</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="258"/>
        <source>Toutes</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="143"/>
        <source>Réessayer</source>
        <translation>Retry</translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="198"/>
        <source>Toutes les tâches</source>
        <translation>All jobs</translation>
    </message>
    <message>
        <location filename="../res/ui/mainwindow.ui" line="27"/>
        <source>État des imprimantes</source>
        <translation>Printer status</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/mainwindow.cpp" line="166"/>
        <location filename="../src/mainwindow.cpp" line="175"/>
        <source>%n élément(s) affiché(s) sur %1</source>
        <translation>
            <numerusform>%n item displayed on %1</numerusform>
            <numerusform>%n items displayed on %1</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/mainwindow.cpp" line="168"/>
        <location filename="../src/mainwindow.cpp" line="177"/>
        <source>%n élément(s)</source>
        <translation>
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>PrinterModel</name>
    <message numerus="yes">
        <location filename="../src/models/printermodel.cpp" line="36"/>
        <source>%n tâche(s) en attente ou en traitement</source>
        <translation>
            <numerusform>%n task pending or processing</numerusform>
            <numerusform>%n tasks pending or processing</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/models/printermodel.cpp" line="38"/>
        <source>Prête à imprimer</source>
        <translation>Ready to print</translation>
    </message>
    <message>
        <location filename="../src/models/printermodel.cpp" line="38"/>
        <source>Indisponible ou en panne</source>
        <translation>Unavailable or broken</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/models/printermodel.cpp" line="40"/>
        <source>%n problème(s) : %1</source>
        <oldsource>Problème(s) : %1</oldsource>
        <translation>
            <numerusform>%n issue: %1</numerusform>
            <numerusform>%n issues: %1</numerusform>
        </translation>
    </message>
</context>
</TS>
