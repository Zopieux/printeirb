# Printeirb

Un petit outil Qt pour consulter les files d'attente et annuler ses jobs facilement sur le réseau de l'ENSEIRB-MATMECA.

## Build

    $ qmake-qt5 CONFIG+=BUILD_ENSEIRB
    $ make
    $ ./printeirb

Traductions disponibles en anglais et français.

## Aperçu

![aperçu 1](https://i.imgur.com/belUiIj.png)

![aperçu 2](https://i.imgur.com/FWZG63h.png)