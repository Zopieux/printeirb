#-------------------------------------------------
#
# Project created by QtCreator 2014-11-21T15:50:44
#
#-------------------------------------------------

QT       += core gui
CONFIG   += static

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = printeirb
TEMPLATE = app

TRANSLATIONS = lang/printeirb_fr.ts \
               lang/printeirb_en.ts

INCLUDEPATH += $$PWD/src $$PWD/lib

BUILD_ENSEIRB {
    unix:LIBS += -l:libcups.so.2 -licui18n
    debug:DESTDIR = build/debug
    release:DESTDIR = build/release
    OBJECTS_DIR = $$DESTDIR/.obj
    MOC_DIR = $$DESTDIR/.moc
    RCC_DIR = $$DESTDIR/.qrc
    UI_DIR = $$DESTDIR/.ui
} else {
    unix:LIBS += -Wl,-Bstatic $$PWD/lib/libcups.a -Wl,-Bdynamic -lz -lgssglue -licui18n
}

SOURCES +=\
    src/utils.cpp \
    src/models/jobmodel.cpp \
    src/models/jobproxysortfiltermodel.cpp \
    src/models/printermodel.cpp \
    src/delegates/nofocusrectdelegate.cpp \
    src/delegates/printerstatusdelegate.cpp \
    src/lpclient.cpp \
    src/mainwindow.cpp \
    src/main.cpp

HEADERS  += \
    src/utils.h \
    src/job.h \
    src/printer.h \
    src/models/jobmodel.h \
    src/models/jobproxysortfiltermodel.h \
    src/models/printermodel.h \
    src/delegates/nofocusrectdelegate.h \
    src/delegates/printerstatusdelegate.h \
    src/lpclient.h \
    src/mainwindow.h

FORMS    += \
    res/ui/mainwindow.ui

RESOURCES += \
    res/res.qrc
